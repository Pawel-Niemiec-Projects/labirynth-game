class GameMap {
    constructor(mazeLength, algorythm) {
        this.mazeLength = mazeLength;
        this._maze = algorythm.createMaze(mazeLength);
    }

    getMaze() {
        return this._maze;
    }

    getSquare(y, x) {

        if (this._maze[y] && this._maze[y][x]) {
            return this._maze[y][x];
        }
        return {
            x: null,
            y: null,
            value: null
        };
    }
}

export default GameMap;