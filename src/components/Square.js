import React from 'react';
import adventurer from './images/adventurer.jpg'
import coinImage from './images/coin.jpg'

const Square = ({square, player, coin}) => {
    const playerImageRepresentation = isPlayerOnTile(square, player) ? (
        <img className={"square"} src={adventurer} alt="Player"/>) : null;
    const coinImageRepresentation = isCoinOnTile(square, coin) ? (
        <img className={"square"} src={coinImage} alt="coin"/>) : null;
    return (
        <div className={`${square.value} square`}>
            {playerImageRepresentation}
            {coinImageRepresentation}
        </div>);
};

const isPlayerOnTile = (square, player) => {
    return player.y === square.y && player.x === square.x;
};

const isCoinOnTile = (square, coin) => {
    if (coin) {
        return coin.y === square.y && coin.x === square.x;
    }
};

export default Square;