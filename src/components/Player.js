class Player {
    constructor(coordinateY, coordinateX) {
        this.y = coordinateY;
        this.x = coordinateX;
    }
}

export default Player;