import React from 'react';
import Maze from "./Maze";
import GameMap from "./GameMap"
import PrimMazeAlgorithm from "./PrimMazeAlgorithm"
import Player from "./Player";
import Coin from "./Coin"

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.gameMap = new GameMap(20, new PrimMazeAlgorithm());
        this.player = this.createPlayer();
        this.state = {
            gameMap: this.gameMap,
            player: this.player,
            coins: this.createCoinArray(4),
        };
    }

    render() {
        return (
            <div className="game" onKeyDown={this.handleArrowKeyPress} tabIndex="0">
                <div className="game-maze">
                    <Maze gameMap={this.state.gameMap}
                          player={this.state.player}
                          coins={this.state.coins}
                    />
                </div>
            </div>)
    }


    createPlayer() {
        let y = Math.floor(Math.random() * this.gameMap.mazeLength);
        let x = Math.floor(Math.random() * this.gameMap.mazeLength);
        if (this.gameMap.getSquare(y, x).value === "empty") {
            return new Player(y, x);
        }
        return this.createPlayer()
    }

    createCoinArray(numberOfCoins) {
        let coinArray = [];
        let node;
        while (numberOfCoins > 0) {
            switch (numberOfCoins % 4) {
                case 0: {
                    node = this.searchForEmptySquareInGivenRadius(0, 0);
                    coinArray.push(new Coin(node.validY, node.validX));
                    numberOfCoins--;
                    break;
                }
                case 1: {
                    node = this.searchForEmptySquareInGivenRadius(0, 10);
                    coinArray.push(new Coin(node.validY, node.validX));
                    numberOfCoins--;
                    break;
                }
                case 2: {
                    node = this.searchForEmptySquareInGivenRadius(10, 0);
                    coinArray.push(new Coin(node.validY, node.validX));
                    numberOfCoins--;
                    break;
                }
                case 3: {
                    node = this.searchForEmptySquareInGivenRadius(10, 10);
                    coinArray.push(new Coin(node.validY, node.validX));
                    numberOfCoins--;
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return coinArray;
    }

    searchForEmptySquareInGivenRadius(x, y) {
        let validY;
        let validX;
        do {
            validY = Math.floor(Math.random() * (10) + y);    //10 is half of maze length so it is allways that. If I ever wanted to change mazeLength so thats possible to be set by player, I need to change it there too
            validX = Math.floor(Math.random() * (10) + x);
        }
        while (this.gameMap.getSquare(validY, validX).value !== "empty"
        && (this.gameMap.getSquare(validY, validX) !== this.gameMap.getSquare(this.player.y, this.player.x)));
        let node = {validY: validY, validX: validX};
        return node;
    }


    handleArrowKeyPress = (event) => {
        const downArrowKey = 38;
        const upArrowKey = 40;
        const leftArrowKey = 37;
        const rightArrowKey = 39;
        let newXCoordinate = this.state.player.x;
        let newYCoordinate = this.state.player.y;
        console.log(this.state.player);
        switch (event.keyCode) {
            case downArrowKey: {
                newYCoordinate--;
                console.log(`player wants to move to up to[${newYCoordinate}, ${newXCoordinate}]`);
                this.tryToMovePlayer(newYCoordinate, newXCoordinate);

                break;
            }
            case upArrowKey: {
                newYCoordinate++;
                console.log(`player wants to move to down to [${newYCoordinate}, ${newXCoordinate}]`);
                this.tryToMovePlayer(newYCoordinate, newXCoordinate);

                break;
            }
            case leftArrowKey: {
                newXCoordinate--;
                console.log(`player wants to move to left to[${newYCoordinate}, ${newXCoordinate}]`);
                this.tryToMovePlayer(newYCoordinate, newXCoordinate);

                break;
            }
            case rightArrowKey: {
                newXCoordinate++;
                console.log(`player wants to move to right to [${[newYCoordinate]}, ${newXCoordinate}]`);
                this.tryToMovePlayer(newYCoordinate, newXCoordinate);

                break;
            }
            default: {

                break;
            }
        }
    };

    tryToMovePlayer(y, x) {
        if (this.gameMap.getSquare(y, x).value === "empty" || this.gameMap.getSquare(y, x).value === "coin") {
            this.movePlayer(y, x)
        }
    }

    movePlayer(y, x) {
        this.setState({player: new Player(y, x)});
        this.removeCoinFromMap(y, x);
    }

    removeCoinFromMap(y, x) {
        if (this.state.coins.filter((coin) => {
                return coin.y === y && coin.x === x
            })[0]) {
            this.setState({
                coins: this.state.coins.filter((coin) => {
                    return (coin.y !== y || coin.x !== x);
                })
            })
        }
    }
}

export default Game;