class Coin {
    constructor(coordinateY, coordinateX) {
        this.y = coordinateY;
        this.x = coordinateX;
    }
}

export default Coin;