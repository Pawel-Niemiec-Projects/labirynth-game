import Square from './Square.js'
import React from 'react';
import './labirynth-game.css';

class Maze extends React.Component {
    render() {
        return this.renderMaze();
    }

    renderMaze() {
        return this.props.gameMap.getMaze().map((row, y) => this.renderRow(row, y))
    }

    renderRow(row, y) {
        return <div className={"row"} key={y}>{row.map((square, x) => this.renderSquare(y, x))}</div>
    }

    renderSquare(y, x) {
        return <Square key={x}
                       square={this.props.gameMap.getSquare(y, x)}
                       player={this.props.player}
                       coin={this.sendCoinToProperSquare(this.props.gameMap.getSquare(y, x), this.props.coins)}/>
    }

    sendCoinToProperSquare(square, coinArray) {
        let returnedCoin = coinArray.filter((coin) => {
            return coin.y === square.y && coin.x === square.x;
        });
        if (returnedCoin[0]) {
            return returnedCoin[0];   //problem with naming :/
        }
        return null;
    }
}

export default Maze;