class PrimMazeAlgorithm {

    createMaze(mazeLength) {
        let maze = Array.from(new Array(mazeLength), (row, yIndex) => {
            return Array.from(new Array(mazeLength), (square, xIndex) => {
                return {
                    value: "wall",
                    x: xIndex,
                    y: yIndex,
                    isVisible: false
                }
            })
        });
        let wallX = [];
        let wallY = [];
        let index;
        let node = {
            x: Math.floor(Math.random() * (mazeLength - 2) + 1),
            y: Math.floor(Math.random() * (mazeLength - 2) + 1),
        };

        sendEmptyValue(node.y, node.x);   //step 2
        wallY.push(node.y - 1);
        wallX.push(node.x);
        wallY.push(node.y + 1);
        wallX.push(node.x);
        wallY.push(node.y);
        wallX.push(node.x - 1);
        wallY.push(node.y);
        wallX.push(node.x + 1);  //end of step 2

        while (wallY.length > 0) {   //step 3
            index = Math.floor(Math.random() * (wallY.length));//same length as wallY so these arrays are actually pairs wall coordinates
            if (checkFrontier(wallY[index], wallX[index])) {
                node.y = wallY[index];
                node.x = wallX[index];
                sendEmptyValue(node.y, node.x);
                createFrontiers(node.y, node.x);
            }
            wallY.splice(index, 1);
            wallX.splice(index, 1);
        }

        function checkFrontier(y, x) {
            let count = 0;
            if (maze[y - 1] && maze[y - 1][x].value === "empty") count++;

            if (maze[y + 1] && maze[y + 1][x].value === "empty") count++;

            if (maze[y] && maze[y][x - 1] && maze[y][x - 1].value === "empty") count++;
            if (maze[y] && maze[y][x + 1] && maze[y][x + 1].value === "empty") count++;
            if (count < 2) return true;

            return false;
        }

        return maze;

        function sendEmptyValue(y, x) {
            maze[y][x] = {
                value: "empty",
                x: x,
                y: y
            }
        }

        function createFrontiers(y, x) {
            if (maze[y - 1] && maze[y - 1][x].value === "wall") {
                if (!isFrontierInList(y - 1, x)) {
                    wallY.push(y - 1);
                    wallX.push(x);
                }
            }
            if (maze[y + 1] && maze[y + 1][x].value === "wall") {
                if (!isFrontierInList(y + 1, x)) {
                    wallY.push(y + 1);
                    wallX.push(x);
                }
            }
            if (maze[y] && maze[y][x - 1] && maze[y][x - 1].value === "wall") {
                if (!isFrontierInList(y, x - 1)) {
                    wallY.push(y);
                    wallX.push(x - 1);
                }
            }
            if (maze[y] && maze[y][x + 1] && maze[y][x + 1].value === "wall") {
                if (!isFrontierInList(y, x + 1)) {
                    wallY.push(y);
                    wallX.push(x + 1);
                }
            }
        }

        function isFrontierInList(y, x) {
            for (let i = 0; i < wallY.length; i++) {
                if ((wallY[i] === y) && (wallX[i] === x)) {
                    return true
                }
            }
            return false
        }
    }
}

export default PrimMazeAlgorithm